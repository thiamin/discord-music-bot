package org.jordan;

public class BotConfig {

    private String token = null;
    private String testGuildId = null;
    private int maxPlayListLength = 100;
    private int maxVolume = 200;

    private subsonicConfig subsonic;

  public String getToken() {
    return token;
  }

  public String getTestGuildId() {
    return testGuildId;
  }

  public int getMaxPlayListLength() {
    return maxPlayListLength;
  }

  public int getMaxVolume() {
    return maxVolume;
  }

  public subsonicConfig getSubsonic() {
    return this.subsonic;
  }

  public String getSubsonicUrl() {
    return subsonic.baseUrl;
  }

  public String getSubsonicPlayerId() {
    return subsonic.playerId;
  }

  public String getSubsonicUsername() {
    return subsonic.username;
  }

  public String getSubsonicPassword() {
    return subsonic.password;
  }

  private static class subsonicConfig { //TODO: Break this out into another class
		public String baseUrl;
    public String playerId;
    public String username;
    public String password;

  }
  }
