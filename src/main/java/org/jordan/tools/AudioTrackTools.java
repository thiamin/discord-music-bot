package org.jordan.tools;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;

public class AudioTrackTools {
  public static String getAudioTrackAsMessage(AudioTrack audioTrack) {
    return getAudioTrackAsMessage(audioTrack.getInfo());
  }

  public static String getAudioTrackAsMessage(AudioTrackInfo audioTrackInfo) {
    var length = audioTrackInfo.length / 1000;
    var lengthMinutes = length / 60;
    var lengthSeconds = length % 60;
    var title = audioTrackInfo.title;
    var author = audioTrackInfo.author;
    return String.format("%s - %s `(%02d:%02d)`", author, title,lengthMinutes, lengthSeconds);
  }

}
