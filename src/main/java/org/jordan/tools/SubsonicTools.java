package org.jordan.tools;

import org.jordan.subsonic.json.Album;
import org.jordan.subsonic.json.Song;

public class SubsonicTools {

  public static String getSongAsMessage(Song song) {
    return String.format("%s - %s ``(%02d:%02d)``",
        song.getArtist(),
        song.getTitle(),
        song.getDuration() / 60,
        song.getDuration() % 60);

  }

  public static String getAlbumAsMessage(Album album) {
    return String.format("%s - %s ``(%02d:%02d)``",
        album.getArtist(),
        album.getName(),
        album.getDuration() / 60,
        album.getDuration() % 60);
  }
}
