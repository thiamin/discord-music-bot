package org.jordan.audio;

import net.dv8tion.jda.api.events.guild.voice.GuildVoiceLeaveEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceMoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class VoiceChannelListener extends ListenerAdapter {

  @Override
  public void onGuildVoiceLeave(GuildVoiceLeaveEvent event) {
    System.out.println("LEFT");
  }

  @Override
  public void onGuildVoiceMove(GuildVoiceMoveEvent event) {
    System.out.println("MOVED");
  }
}
