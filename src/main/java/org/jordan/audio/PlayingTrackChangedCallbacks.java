package org.jordan.audio;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

public interface PlayingTrackChangedCallbacks {

  void onTrackEnd(AudioTrack audioTrack);

  void onTrackStart(AudioTrack audioTrack);

  void onTrackException(AudioTrack audioTrack);

}
