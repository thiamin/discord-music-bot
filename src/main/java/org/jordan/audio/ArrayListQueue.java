package org.jordan.audio;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

public class ArrayListQueue<T> {

  ArrayDeque<T> arrayDeque = new ArrayDeque<>();

  public void add(T e) {
    arrayDeque.add(e);
  }

  public T getNext() {
    T next = null;
    if (hasNext()) {
      next = arrayDeque.pollFirst();
    }
    return next;
  }

  public boolean hasNext() {
    return !arrayDeque.isEmpty();
  }

  public List<T> getAsList() { //TODO: maybe use deepclone
    return new ArrayList<>(arrayDeque);
  }

  public int getNextElementIndex() {
    return 0;
  }

  public void clear() {
    arrayDeque.clear();
  }

  public boolean isLooping() {
    return false;
  }

  public void setLooping(boolean looping) {
    //this.looping = looping;
  }

}