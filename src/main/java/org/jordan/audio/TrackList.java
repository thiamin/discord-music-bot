package org.jordan.audio;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrackList extends AudioEventAdapter {

  private final ArrayListQueue<AudioTrack> trackQueue = new ArrayListQueue<>();
  private final Logger log;

  PlayingTrackChangedCallbacks playingTrackChangedCallbacks;

  public TrackList(PlayingTrackChangedCallbacks playingTrackChangedCallbacks) {
    this.log = LoggerFactory.getLogger(TrackList.class);
    this.playingTrackChangedCallbacks = playingTrackChangedCallbacks;
  }

  public void clear() {
    trackQueue.clear();
  }

  public void queue(AudioTrack audioTrack) {
    trackQueue.add(audioTrack);
  }

  public AudioTrack getNext() {
    return trackQueue.getNext();
  }

  public boolean hasNext() {
    return trackQueue.hasNext();
  }


  public AudioTrack[] getTrackList(int maxNumber) {
    List<AudioTrack> audioTrackList = trackQueue.getAsList();
    int size = Math.min(maxNumber, audioTrackList.size());
    return audioTrackList.toArray(new AudioTrack[size]);
  }

  public int getTrackListLocation() {
    return trackQueue.getNextElementIndex();
  }

  public void setLooping(boolean looping) {
    trackQueue.setLooping(looping);
  }

  public boolean isLooping() {
    //return trackQueue.getLoop();
    return trackQueue.isLooping();
  }

  @Override
  public void onPlayerPause(AudioPlayer player) {
    // Player was paused
  }

  @Override
  public void onPlayerResume(AudioPlayer player) {
    // Player was resumed
  }

  @Override
  public void onTrackStart(AudioPlayer player, AudioTrack track) {
    // A track started playing
    log.info("Started track (" + track.getInfo().uri + ") [" + track + "]");
    playingTrackChangedCallbacks.onTrackStart(track);
  }

  @Override
  public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason) {
    log.info("INFO: Track '" + track + "' ended. Reason: " + endReason.toString());
    AudioTrack playingTrack = null;
    if (endReason.mayStartNext) {
      log.info("Starting next track");
      if (trackQueue.hasNext()) {
        playingTrack = trackQueue.getNext();
        player.startTrack(playingTrack, false);
      } else {
        player.stopTrack();
      }
    }

    playingTrackChangedCallbacks.onTrackEnd(playingTrack);

    // endReason == FINISHED: A track finished or died by an exception (mayStartNext = true).
    // endReason == LOAD_FAILED: Loading of a track failed (mayStartNext = true).
    // endReason == STOPPED: The player was stopped.
    // endReason == REPLACED: Another track started playing while this had not finished
    // endReason == CLEANUP: Player hasn't been queried for a while, if you want you can put a
    //                       clone of this back to your queue
  }

  @Override
  public void onTrackException(AudioPlayer player, AudioTrack track, FriendlyException exception) {

    playingTrackChangedCallbacks.onTrackException(track);

    if (trackQueue.hasNext()) {
      player.startTrack(trackQueue.getNext(), false);
    }

    log.error("Track exception " + exception.getMessage());
  }

  @Override
  public void onTrackStuck(AudioPlayer player, AudioTrack track, long thresholdMs) {
    // Audio track has been unable to provide us any audio, might want to just start a new track

    log.error("Track stuck " + track.getInfo());
  }

}
