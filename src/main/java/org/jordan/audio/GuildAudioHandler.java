package org.jordan.audio;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import org.jordan.ui.ErrorEmbedBuilder;
import org.jordan.ui.RegularEmbedBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


public class GuildAudioHandler {

  private final AudioPlayerManager playerManager;
  private final AudioPlayer audioPlayer;
  private final BotAudioSendHandler botAudioSendHandler;
  private final MessageChannel messageChannel; /* textChannel to send audio update events to (ie now playing). */
  private final TrackList trackList;

  private final Logger log;


  public GuildAudioHandler(AudioPlayerManager manager, MessageChannel messageChannel) {
    log = LoggerFactory.getLogger(GuildAudioHandler.class);
    log.info("GuildAudioHandler created.");

    this.messageChannel = messageChannel;
    playerManager = manager;
    audioPlayer = manager.createPlayer();
    botAudioSendHandler = new BotAudioSendHandler(audioPlayer);
    trackList = new TrackList(new NextPlayingTrackCallback());
    audioPlayer.addListener(trackList);
  }

  public int skipTrack(int n) {
    if (n < 1) {
      return 0;
    }

    log.info("Skipping " + n + " tracks.");
    int skipCount = 0;
    AudioTrack playingTrack = null;
    while (trackList.hasNext() && skipCount < n) {
      playingTrack = trackList.getNext();
      skipCount++;
    }

    if (playingTrack == null) {
      log.info("Cannot skip tracks, playlist already empty");
      audioPlayer.stopTrack();
      trackList.clear();
    } else {
      audioPlayer.startTrack(playingTrack, false);
    }

    return skipCount;

  }

  public int skipTrack() {
    return skipTrack(1);
  }

  public void addTrack(AudioPlaylist playlist) {
    List<AudioTrack> tl = playlist.getTracks();
    for (AudioTrack t : tl) {
      System.out.println(t.getInfo());
      trackList.queue(t);
    }

    if (audioPlayer.getPlayingTrack() == null || audioPlayer.isPaused()) {
      audioPlayer.startTrack(trackList.getNext(), false);
    }

    log.info(audioPlayer.toString());
    log.info(audioPlayer.getPlayingTrack().toString());
  }

  public void addTrack(AudioTrack audioTrack) {
    trackList.queue(audioTrack);
    if (audioPlayer.getPlayingTrack() == null) {
      audioPlayer.startTrack(trackList.getNext(), false);
    }

    log.info(audioPlayer.toString());
    log.info(audioPlayer.getPlayingTrack().toString());
  }

  public void addTrack(String identifier) {
    playerManager.loadItem(identifier, new AudioLoadResultHandler() {
      @Override
      public void trackLoaded(AudioTrack track) {
        trackList.queue(track);

        if (audioPlayer.getPlayingTrack() == null) {
          audioPlayer.startTrack(trackList.getNext(), false);
        }
      }

      @Override
      public void playlistLoaded(AudioPlaylist playlist) {
        List<AudioTrack> tl = playlist.getTracks();
        for (AudioTrack t : tl) {
          System.out.println(t.getInfo());
          trackList.queue(t);
        }

        if (audioPlayer.getPlayingTrack() == null || audioPlayer.isPaused()) {
          audioPlayer.startTrack(trackList.getNext(), false);
        }
      }

      @Override
      public void noMatches() {
        System.err.println("No matches");
      }

      @Override
      public void loadFailed(FriendlyException exception) {
        System.err.println("Fatal error loading track");
      }
    });
  }

  public void setLooping(boolean looping) {
    trackList.setLooping(looping);
  }

  public boolean isLooping() {
    return trackList.isLooping();
  }

  public AudioPlayer getAudioPlayer() {
    return audioPlayer;
  }

  public AudioPlayerManager getPlayerManager() {
    return playerManager;
  }

  public BotAudioSendHandler getBotAudioSendHandler() {
    return botAudioSendHandler;
  }

  // TODO: don't do a big reallocation, maybe create interface using AudioTrack that can only read info...
  public AudioTrackInfo[] getTrackList(int maxSize) {
    var audioTrackArray = trackList.getTrackList(maxSize);
    AudioTrackInfo[] trackInfo;
    int offset = 0;
    if (audioPlayer.getPlayingTrack() != null) {
      trackInfo = new AudioTrackInfo[audioTrackArray.length + 1];
      trackInfo[0] = audioPlayer.getPlayingTrack().getInfo();
      offset = 1;
    } else {
      trackInfo = new AudioTrackInfo[audioTrackArray.length];
    }

    for (int i = 0; i < audioTrackArray.length; i++) {
      trackInfo[i + offset] = audioTrackArray[i].getInfo();
    }

    return trackInfo;
  }

  public int getTrackListLocation() {
    return trackList.getTrackListLocation();
  }

  public void setVolume(int vol) {
    audioPlayer.setVolume(vol);
  }

  public void clearQueue() {
    audioPlayer.stopTrack();
    trackList.clear();
  }

  public boolean isPaused() {
    return audioPlayer.isPaused();
  }

  public void setPaused(boolean value) {
    audioPlayer.setPaused(value);
  }

  private class NextPlayingTrackCallback implements PlayingTrackChangedCallbacks {

    @Override
    public void onTrackEnd(AudioTrack audioTrack) {
      // Do nothing.
    }

    @Override
    public void onTrackStart(AudioTrack audioTrack) {
      EmbedBuilder eb = new RegularEmbedBuilder();
      eb.setTitle("Now playing");
      eb.setDescription(
          "[" + audioTrack.getInfo().title + "]" + "(" + audioTrack.getInfo().uri + ")");
      messageChannel.sendMessageEmbeds(eb.build()).queue();
    }

    @Override
    public void onTrackException(AudioTrack audioTrack) {
      EmbedBuilder eb = new ErrorEmbedBuilder();
      eb.setTitle("Error playing track");
      eb.setDescription(
          "[" + audioTrack.getInfo().title + "]" + "(" + audioTrack.getInfo().uri + ")."
              + "Skipping.");
      messageChannel.sendMessageEmbeds(eb.build()).queue();
    }

  }

}
