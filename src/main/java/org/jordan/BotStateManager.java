package org.jordan;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import java.util.concurrent.ConcurrentHashMap;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.channel.concrete.VoiceChannel;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;

/* Class that contains and handles stateful data about all active guilds. eg, voice, buttons, etc */
public class BotStateManager {
  final ConcurrentHashMap<Long, GuildManager> guildManagers;
  final AudioPlayerManager audioPlayerManager;
  final JDA jda;

  public BotStateManager(JDA jda, AudioPlayerManager audioPlayerManager) {
    guildManagers = new ConcurrentHashMap<>();
    this.audioPlayerManager = audioPlayerManager;
    this.jda = jda;
  }

  public void removeGuildManager(long id) {
    //var guildManger = guildManagers.get(id);
    //if (guildManger != null) {
    //  guildManger.destroyGuildAudioHandler();
    //}
    //guildManagers.remove(id);
  }

  public GuildManager createGuildManager(long id, MessageChannel messageChannel) {
    var guildManager = new GuildManager(jda, id, messageChannel, audioPlayerManager);
    guildManagers.put(id, guildManager);
    return guildManager;
  }

  public GuildManager getGuildManager(long id) {
    return guildManagers.get(id);
  }

  public AudioPlayerManager getPlayerManager() {
    return audioPlayerManager;
  }

}
