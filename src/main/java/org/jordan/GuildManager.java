package org.jordan;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.channel.concrete.VoiceChannel;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import net.dv8tion.jda.api.managers.AudioManager;
import org.jordan.audio.GuildAudioHandler;

public class GuildManager {
  final MessageChannel messageChannel;
  final AudioPlayerManager audioPlayerManager;
  private final long guildId;
  final JDA jda;

  GuildAudioHandler guildAudioHandler;

  public GuildManager(JDA jda, long guildId, MessageChannel messageChannel, AudioPlayerManager audioPlayerManager) {
    this.messageChannel = messageChannel;
    this.audioPlayerManager = audioPlayerManager;
    this.guildId = guildId;
    this.jda = jda;
  }

  private void createGuildAudioHandler() {
    guildAudioHandler = new GuildAudioHandler(audioPlayerManager, messageChannel);
  }

  public GuildAudioHandler getGuildAudioHandler() {
    return guildAudioHandler;
  }

  public boolean joinVoiceChannel(VoiceChannel voiceChannel) {
    if (guildAudioHandler == null) {
      createGuildAudioHandler();
    }

    Guild g = voiceChannel.getJDA().getGuildById(guildId);
    if (g == null) {
      return false;
    }

    AudioManager audioManager = g.getAudioManager();
    audioManager.setSendingHandler(guildAudioHandler.getBotAudioSendHandler());
    audioManager.openAudioConnection(voiceChannel);

    return true;
  }

  public void destroyGuildAudioHandler() {
    if (guildAudioHandler == null) {
      return;
    }

    AudioManager audioManager = jda.getGuildById(this.guildId).getAudioManager();
    audioManager.closeAudioConnection();
    guildAudioHandler.getAudioPlayer().destroy();
    audioManager.setSendingHandler(null);
    guildAudioHandler = null;
  }

}
