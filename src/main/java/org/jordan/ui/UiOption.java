package org.jordan.ui;

import java.util.UUID;
import net.dv8tion.jda.api.interactions.components.buttons.ButtonStyle;

public class UiOption<T> {
  ButtonStyle buttonStyle;
  final UUID uuid;
  private String printableName;
  private T optionValue;

  public UiOption() {
    uuid = UUID.randomUUID();
  }

  public UiOption(ButtonStyle buttonStyle, String printableName, T optionValue) {
    uuid = UUID.randomUUID();
    this.buttonStyle = buttonStyle;
    this.printableName = printableName;
    this.optionValue = optionValue;
  }

  public String getPrintableName() {
    return printableName;
  }

  public void setPrintableName(String printableName) {
    this.printableName = printableName;
  }

  public T getOptionValue() {
    return optionValue;
  }

  public void setOptionValue(T optionValue) {
    this.optionValue = optionValue;
  }

  public UUID getUuid() {
    return this.uuid;
  }

  public ButtonStyle getButtonStyle() {
    return buttonStyle;
  }
}
