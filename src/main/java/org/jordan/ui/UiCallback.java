package org.jordan.ui;

import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;

public interface UiCallback<T> {

  void onButtonClick(T value, ButtonInteractionEvent event);
}
