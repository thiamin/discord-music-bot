package org.jordan.ui;

import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jordan.ui.ButtonEventHandler;

import java.util.*;

public class UiListener extends ListenerAdapter {

  HashMap<UUID, ButtonEventHandler> buttonHandlerMap = new HashMap<>();

  @Override
  public void onButtonInteraction(ButtonInteractionEvent event) {
    assert event.getComponent().getId() != null;
    UUID eventId = UUID.fromString(event.getComponent().getId());
    ButtonEventHandler selectOptionBuilder = buttonHandlerMap.get(eventId);
    if (selectOptionBuilder == null) {
      event.reply("FAILED TO FIND EVENT HANDLER...").queue();
      event.getMessage().delete().queue();
      return;
    }

    selectOptionBuilder.onButtonInteraction(event);
  }

  public void registerButtonEventHandler(ButtonEventHandler selectOptionBuilder) {
    for (UUID uuid : selectOptionBuilder.getButtonUuids()) {
      buttonHandlerMap.put(uuid, selectOptionBuilder);
    }
  }

  public void deregisterButtonEventHandler(ButtonEventHandler selectOptionBuilder) {
    for (UUID uuid : selectOptionBuilder.getButtonUuids()) {
      ButtonEventHandler handler = buttonHandlerMap.get(uuid);
      buttonHandlerMap.remove(uuid);
      handler.destroy();
    }
  }

}
