package org.jordan.ui;

import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;

import java.util.List;
import java.util.UUID;

public interface ButtonEventHandler {

  void onButtonInteraction(ButtonInteractionEvent event);
  List<UUID> getButtonUuids();
  void destroy();
}
