package org.jordan.ui;

import java.awt.Color;
import net.dv8tion.jda.api.EmbedBuilder;

public class ErrorEmbedBuilder extends EmbedBuilder {

  public ErrorEmbedBuilder() {
    super.setColor(Color.RED);
  }
}
