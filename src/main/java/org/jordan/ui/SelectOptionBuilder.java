package org.jordan.ui;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.interactions.InteractionHook;
import net.dv8tion.jda.api.interactions.components.ActionRow;
import net.dv8tion.jda.api.interactions.components.buttons.Button;
import net.dv8tion.jda.api.interactions.components.buttons.ButtonStyle;
import org.jordan.Bot;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/* TODO: start making class async.
 *  Possibly split handling of regular messages and InteractionHooks into separate classes.
 *  Add watcher to auto-delete un-clicked selectOptionBuilder(s)
 */
/* Class to create a text message with clickable buttons and register callbacks */
public class SelectOptionBuilder<T> implements ButtonEventHandler {

  public final int MAX_BUTTON_COUNT = Message.MAX_COMPONENT_COUNT * Message.MAX_COMPONENT_COUNT - 1;

  private final UiListener uiListener;
  ArrayList<UiOption<T>> optionArrayList = new ArrayList<>();

  InteractionHook interactionHook;
  UiCallback<T> callback;
  private String messageContents;


  public SelectOptionBuilder(UiListener uiListener, InteractionHook interactionHook,
      UiCallback<T> callback) {
    this.uiListener = uiListener;
    this.interactionHook = interactionHook;
    this.callback = callback;
  }

  public void addUiOption(UiOption<T> uiOption) {
    optionArrayList.add(uiOption);
  }

  public void addUiOption(List<UiOption<T>> uiOptions) {
    optionArrayList.addAll(uiOptions);
  }

  public void addMessageText(String text) {
    this.messageContents = text;
  }

  public void send() {
    List<Button> buttonArrayList = new ArrayList<>();

    for (UiOption<T> uiOption : optionArrayList) {
      buttonArrayList.add(
          Button.of(uiOption.getButtonStyle(),
              uiOption.getUuid().toString(),
              uiOption.getPrintableName()));
    }

    /* Partition into multiples of 5. */
    //List<ActionRow> actionRows = new ArrayList<>();
    //for (int i = 0; i < buttonArrayList.size(); i = i + 5) {
    //  int end = Math.min(i + 5, buttonArrayList.size()); /* End will be exclusive */
    //  ActionRow.of(buttonArrayList.subList(i,end)).
    //  actionRows.add(ActionRow.of(buttonArrayList.subList(i, end)));
    //}

    List<ActionRow> actionRows = ActionRow.partitionOf(buttonArrayList);

    if (messageContents != null) {
      interactionHook.editOriginal(messageContents).queue();
    }

    interactionHook.editOriginalComponents(actionRows).queue();

    uiListener.registerButtonEventHandler(this);
  }

  @Override
  public List<UUID> getButtonUuids() {
    ArrayList<UUID> response = new ArrayList<>();
    for (UiOption<?> uiOption : optionArrayList) {
      response.add(uiOption.getUuid());
    }

    return response;
  }

  @Override
  public void onButtonInteraction(ButtonInteractionEvent event) {
    if (interactionHook != null) {
      interactionHook.deleteOriginal().queue();
    }

    UiOption<T> eventUiOption = null;
    String buttonUuid = event.getComponent().getId();

    for (UiOption<T> uiOption : optionArrayList) {
      if (uiOption.getUuid().toString().equals(buttonUuid)) {
        eventUiOption = uiOption;
        break;
      }
    }

    if (eventUiOption != null) {
      callback.onButtonClick(eventUiOption.getOptionValue(), event);
    }

    uiListener.deregisterButtonEventHandler(this);
  }

  @Override
  public void destroy() {
  }
}
