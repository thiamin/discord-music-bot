package org.jordan.ui;

import net.dv8tion.jda.api.EmbedBuilder;

import java.awt.*;

public class RegularEmbedBuilder extends EmbedBuilder {

  public RegularEmbedBuilder() {
    super.setColor(Color.GREEN);
  }
}
