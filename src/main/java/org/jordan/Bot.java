package org.jordan;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import java.io.File;
import java.io.IOException;
import java.util.List;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.JDA;


import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import net.dv8tion.jda.api.requests.GatewayIntent;
import org.jordan.commands.*;
import org.jordan.subsonic.SubsonicApiWrapper;
import org.jordan.ui.UiListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class Bot {

  private final Logger log;
  private final BotConfig botConfig;

  JDA api;
  private final AudioPlayerManager playerManager = new DefaultAudioPlayerManager();
  private final BotStateManager botStateManager;

  private final UiListener uiListener = new UiListener();
  private final CommandListener commandListener = new CommandListener();

  private final SubsonicApiWrapper subsonic;

  public Bot(BotConfig botConfig) {
    log = LoggerFactory.getLogger(this.getClass());
    log.info("Starting music bot.");

    this.botConfig = botConfig;
    AudioSourceManagers.registerLocalSource(playerManager);
    if (botConfig.getSubsonic() == null) {
      log.info("No subsonic data in config. Subsonic support disabled");
      subsonic = null;
    } else {
      subsonic = new SubsonicApiWrapper(
          botConfig.getSubsonicUrl(),
          botConfig.getSubsonicPlayerId(),
          botConfig.getSubsonicUsername(),
          botConfig.getSubsonicPassword());
      playerManager.registerSourceManager(subsonic.getSourceManager());
      //playerManager.registerSourceManager(new SubsonicAudioSourceManager(botConfig.getSubsonicUrl(), botConfig.getSubsonicPlayerId(), botConfig.getSubsonicUsername(),
      //    botConfig.getSubsonicPassword()));
    }
    AudioSourceManagers.registerRemoteSources(playerManager);

    JDABuilder jBuilder = JDABuilder.createDefault(botConfig.getToken());
    jBuilder.enableIntents(GatewayIntent.MESSAGE_CONTENT, GatewayIntent.GUILD_WEBHOOKS,
        GatewayIntent.GUILD_VOICE_STATES);
    api = jBuilder.build();
    api.addEventListener(commandListener);
    api.addEventListener(uiListener);
    //api.addEventListener(new VoiceChannelListener());
    Guild testServer = null;

    try {
      api.awaitReady();
      testServer = api.getGuildById(botConfig.getTestGuildId());
    } catch (Exception e) {
      System.err.println("Fatal error starting music bot");
      System.err.println(e.getMessage());
      System.exit(1);
    }

    botStateManager = new BotStateManager(api, playerManager);

    if (testServer == null) {
      log.error("FATAL: Test guild ID invalid");
      System.exit(1);
    }

    registerCommands(testServer);
    configureBotStatus();
    System.out.println(api.getInviteUrl(Permission.VOICE_CONNECT, Permission.MESSAGE_EMBED_LINKS,
        Permission.VIEW_CHANNEL, Permission.MESSAGE_SEND, Permission.VOICE_SPEAK));
  }

  private void configureBotStatus() {
    var manager = api.getSelfUser().getManager();
    manager.setName("JBMB").queue();

    File avatar = new File("logo.png");
    if (!avatar.exists()) {
      return;
    }

    try {
      manager.setAvatar(Icon.from(avatar)).queue();
    } catch (IOException e) {
      log.error("Failed to load discord bot image. Message: '" + e.getMessage() + "'.");
    }

  }

  private void registerCommands(Guild g) {

    final List<InterfaceCmd> commands = new ArrayList<>();
    commands.add(new JoinCmd(botStateManager));
    commands.add(new LeaveCmd(botStateManager));
    commands.add(new SkipCmd(botStateManager));
    commands.add(new QueueCmd(botStateManager));
    commands.add(new VolumeCmd(botStateManager));
    commands.add(new ClearCmd(botStateManager));
    commands.add(new PlayCmd(botStateManager));
    commands.add(new PauseCmd(botStateManager));
    commands.add(new YouTubeCmd(botStateManager, uiListener));
    commands.add(new SubsonicCmd(botStateManager, uiListener, subsonic));

    ArrayList<SlashCommandData> slashCommandData = new ArrayList<>();
    for (InterfaceCmd cmd : commands) {
      slashCommandData.add(cmd.getSlashCommand());
    }

    api.updateCommands().addCommands(slashCommandData).queue();

    if (g != null) {
      g.updateCommands().addCommands(slashCommandData).queue();
    }

    commandListener.registerCommand(commands);
  }

  public JDA getApi() {
    return api;
  }

  public static void main(String[] args) {

    String configPath = "config.json";

    for (int i = 0; i < args.length; i++) {
      if (args[i].equals("-c")) {
        if (i == args.length - 1) {
          System.out.println("Config file not specified.");
          System.exit(0);
        } else {
          i++;
          configPath = args[i];
        }
      }
    }

    File configFile = new File(configPath);
    if (!configFile.exists()) {
      System.out.println("Config file '" + configPath + "' not found.");
      System.exit(0);
    }

    ObjectMapper jsonReader = new ObjectMapper();
    BotConfig botConfig = null;
    try {
      botConfig = jsonReader.readValue(configFile, BotConfig.class);
    } catch (IOException e) {
      System.err.println(
          "Error opening config file '" + configPath + "'. Message: '" + e.getMessage() + "'");
      System.exit(1);
    }

    if (botConfig == null) {
      System.err.println("Unknown error opening config file. Got null json deserialization.");
      System.exit(1);
    }

    Bot bot = new Bot(botConfig);
  }
}