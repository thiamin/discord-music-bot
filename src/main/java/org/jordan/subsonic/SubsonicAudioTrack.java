package org.jordan.subsonic;

import com.sedmelluq.discord.lavaplayer.container.ogg.OggAudioTrack;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException.Severity;
import com.sedmelluq.discord.lavaplayer.tools.io.NonSeekableInputStream;
import com.sedmelluq.discord.lavaplayer.tools.io.SeekableInputStream;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import com.sedmelluq.discord.lavaplayer.track.DelegatedAudioTrack;
import com.sedmelluq.discord.lavaplayer.track.playback.LocalAudioTrackExecutor;
import org.jordan.subsonic.SubsonicClient;

public class SubsonicAudioTrack extends DelegatedAudioTrack {
  final SubsonicClient client;

  public SubsonicAudioTrack(AudioTrackInfo trackInfo, SubsonicClient client) {
    super(trackInfo);
    this.client = client;
  }

  @Override
  public void process(LocalAudioTrackExecutor executor) throws Exception {
    var stream = client.stream(trackInfo.identifier, 120, "opus").join();
    if (stream == null || stream.getResponse() == null || stream.getResponse().getBuffer() == null) {

      String subsonicError = "server not accessible";
      if (stream.getSubsonicError() != null) {
        subsonicError = stream.getSubsonicError().getMessage();
      }

      throw new FriendlyException("Got null stream response from subsonic", Severity.FAULT, new Exception(subsonicError));
    }

    var track = new OggAudioTrack(trackInfo, new NonSeekableInputStream(stream.getResponse().getBuffer()));
    processDelegate(track, executor);
  }
}
