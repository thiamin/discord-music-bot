package org.jordan.subsonic;//package org.jordan.subsonic;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManager;
import com.sedmelluq.discord.lavaplayer.track.AudioItem;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioReference;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import com.sedmelluq.discord.lavaplayer.track.BasicAudioPlaylist;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jordan.subsonic.api.response.ApiResponse;
import org.jordan.subsonic.json.Album;
import org.jordan.subsonic.json.Playlist;
import org.jordan.subsonic.json.SearchResult;
import org.jordan.subsonic.json.Song;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SubsonicAudioSourceManager implements AudioSourceManager {

  private final Logger log = LoggerFactory.getLogger(SubsonicAudioSourceManager.class);

  private final String songSearchPrefix = "subsonic:";
  private final String songIdPrefix = SubsonicAudioTypes.SONG.name().toLowerCase() + ":";
  private final String albumIdPrefix = SubsonicAudioTypes.ALBUM.name().toLowerCase() + ":";
  private final Pattern urlPattern;

  private final String baseUrl;
  private final SubsonicClient client;

  public SubsonicAudioSourceManager(String baseUrl, SubsonicClient client) {
    this.baseUrl = baseUrl;
    this.client = client;

    this.urlPattern = Pattern.compile(
        baseUrl.concat("app/#/(playlist|album)/(.*)/show"));
  }

  @Override
  public String getSourceName() {
    return "subsonic";
  }

  @Override
  public AudioItem loadItem(AudioPlayerManager manager, AudioReference reference) {
    if (reference.identifier.startsWith(songSearchPrefix)
        && reference.identifier.length() > songSearchPrefix.length()) {
      String searchQuery = reference.identifier.substring(songSearchPrefix.length() + 1);
      return loadSongSearch(searchQuery);
    }

    if (reference.identifier.startsWith(albumIdPrefix) && reference.identifier.length() > albumIdPrefix.length()) {
      System.out.println("ALBUM");
      String albumId = reference.identifier.substring(albumIdPrefix.length() + 1).trim();
      return loadAlbum(albumId);
    }

    if (reference.identifier.startsWith(songIdPrefix) && reference.identifier.length() > songIdPrefix.length()) {
      System.out.println("SONG");
      String songId = reference.identifier.substring(songIdPrefix.length() + 1).trim();
      return loadSong(songId);
    }

    Matcher matcher = urlPattern.matcher(reference.identifier);
    if (matcher.matches()) {
      if (matcher.group(1).equals("playlist")) {
        return loadPlaylist(matcher.group(2));
      } else {
        return loadAlbum(matcher.group(2));
      }
    }
    return null;

  }

  public AudioPlaylist loadAlbum(String id) {
    ApiResponse<Album> album = client.getAlbum(id).join();
    if (album != null && album.getResponse() != null && album.getResponse().getSongList() != null) {
      return createPlaylist(album.getResponse().getName(), album.getResponse().getSongList(),
          false);
    }
    return null;
  }

  public AudioPlaylist loadPlaylist(String id) {
    ApiResponse<Playlist> response = client.getPlaylist(id).join();
    System.out.println("HELLO?");
    if (response != null && response.getResponse() != null
        && response.getResponse().getSongList() != null) {

      return createPlaylist(response.getResponse().getName(), response.getResponse().getSongList(),
          false);
    }
    return null;
  }

  public AudioPlaylist loadSongSearch(String search) {
    ApiResponse<SearchResult> sr = client.search(search).join();
    if (sr != null && sr.getResponse() != null && sr.getResponse().getSong() != null) {
      return createPlaylist(search, sr.getResponse().getSong(), true);
    }
    return null;
  }

  public SubsonicAudioTrack loadSong(String id) {
    System.out.println("'" + id + "'");
    ApiResponse<Song> song = client.getSong(id).join();
    if (song == null || song.getSubsonicError() != null) {
      log.error("Failed to load song '" + id + "'");
      return null;
    }
    var trackInfo = createTrackInfo(song.getResponse());
    return new SubsonicAudioTrack(trackInfo, client);
  }


  private AudioPlaylist createPlaylist(String name, List<Song> songs, boolean isSearchResult) {
    List<AudioTrack> playlist = new ArrayList<>();
    for (var song : songs) {
      var trackInfo = createTrackInfo(song);
      playlist.add(new SubsonicAudioTrack(trackInfo, client));
    }
    return new BasicAudioPlaylist(name, playlist, playlist.get(0), isSearchResult);
  }

  //private AudioItem loadPlaylist(String playlistId) {
  //}

  @Override
  public boolean isTrackEncodable(AudioTrack track) {
    return false;
  }

  @Override
  public void encodeTrack(AudioTrack track, DataOutput output) throws IOException {

  }

  @Override
  public AudioTrack decodeTrack(AudioTrackInfo trackInfo, DataInput input) throws IOException {
    return null;
  }

  @Override
  public void shutdown() {

  }

  private String songSearchToUrl(String id) { //FIXME: use correct URI encoding.

    return baseUrl.concat("app/#/song?filter={%22title%22%3A%22").concat(id.replace(" ", "%20"))
        .concat("%22}");
  }

  private String playlistIdToUrl(String id) {
    return baseUrl.concat("app/#/playlist/").concat(id.replace(" ", "%20")).concat("/show");
  }


  private AudioTrackInfo createTrackInfo(Song song) {
    return new AudioTrackInfo(song.getTitle(), song.getArtist(), (long) 1000 * song.getDuration(),
        song.getId(), true, songSearchToUrl(song.getTitle()));
  }
}
