package org.jordan.subsonic;

public class SubsonicAudioItem {
  final SubsonicAudioTypes type;
  final String id;

  public SubsonicAudioItem(String id, SubsonicAudioTypes type) {
    this.id = id;
    this.type = type;
  }

  public String getAsString() {
    return type.toString().toLowerCase() + ": " + id;
  }

  public String getId() {
    return id;
  }

  public SubsonicAudioTypes getType() {
    return type;
  }
}
