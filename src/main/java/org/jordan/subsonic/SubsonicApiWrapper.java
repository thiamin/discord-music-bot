package org.jordan.subsonic;

import com.sedmelluq.discord.lavaplayer.source.AudioSourceManager;
import org.jordan.subsonic.api.response.ApiResponse;
import org.jordan.subsonic.json.SearchResult;

public class SubsonicApiWrapper {
  private final String baseUrl;
  private final SubsonicClient client;
  private final SubsonicAudioSourceManager subsonicAudioSourceManager;

  public SubsonicApiWrapper(String baseUrl, String clientId, String username,
      String password) {

    if (!baseUrl.endsWith("/")) {
      baseUrl = baseUrl.concat("/");
    }

    this.baseUrl = baseUrl;

    client = new SubsonicClient(baseUrl.concat("rest/"), clientId, "1.16.1", username, password);
    subsonicAudioSourceManager = new SubsonicAudioSourceManager(baseUrl, client); //TODO: edit api in other project to have client.getBaseUrl

  }

  public SearchResult search(String query) {
    ApiResponse<SearchResult> apiResponse = client.search(query).join();
    if (apiResponse == null || apiResponse.getSubsonicError() != null) {
      return null;
    }
    System.out.println("HELLO!");
    return apiResponse.getResponse();
  }

  public SubsonicAudioSourceManager getSourceManager() {
    return subsonicAudioSourceManager;
  }
}
