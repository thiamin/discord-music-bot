package org.jordan.subsonic;

public enum SubsonicAudioTypes {
  PLAYLIST,
  ALBUM,
  SONG
}
