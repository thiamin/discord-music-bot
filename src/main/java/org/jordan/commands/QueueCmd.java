package org.jordan.commands;

import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import net.dv8tion.jda.api.entities.emoji.Emoji;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import org.jordan.BotStateManager;
import org.jordan.GuildManager;
import org.jordan.tools.AudioTrackTools;

public class QueueCmd implements InterfaceCmd {

  private final BotStateManager stateManager;

  public QueueCmd(BotStateManager stateManager) {
    this.stateManager = stateManager;
  }

  @Override
  public SlashCommandData getSlashCommand() {
    return Commands.slash("queue", "Get track list");
  }

  @Override
  public void trigger(SlashCommandInteractionEvent event) {

    GuildManager guildManager = stateManager.getGuildManager(event.getGuild().getIdLong());
    if (guildManager == null || guildManager.getGuildAudioHandler() == null) {
      event.reply("Bot not in any audio channels.").queue();
      return;
    }

    AudioTrackInfo[] trackInfo = guildManager.getGuildAudioHandler().getTrackList(10);
    StringBuilder response = new StringBuilder();

    if (trackInfo.length == 0) {
      response.append("Queue is empty.");
    }
    for (int i = 0; i < trackInfo.length; i++) {
      if (i == guildManager.getGuildAudioHandler().getTrackListLocation()) {
        response.append("`›");
      } else {
        response.append("` ");
      }
      response.append(String.format("[%02d]`", i))
          .append(AudioTrackTools.getAudioTrackAsMessage(trackInfo[i])).append("\n");

    }

    event.reply(response.toString()).queue();
  }
}
