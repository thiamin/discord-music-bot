package org.jordan.commands;

import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import org.jordan.BotStateManager;

public class LeaveCmd implements InterfaceCmd {

  private final BotStateManager stateManager;

  public LeaveCmd(BotStateManager stateManager) {
    this.stateManager = stateManager;
  }

  @Override
  public SlashCommandData getSlashCommand() {
    return Commands.slash("leave", "Force music bot out of channel");
  }

  @Override
  public void trigger(SlashCommandInteractionEvent event) {
    var guildManager = stateManager.getGuildManager(event.getGuild().getIdLong());
    if (guildManager == null || guildManager.getGuildAudioHandler()
     == null) {
      event.reply("Bot not in any audio channels.").queue();
    } else {
      guildManager.destroyGuildAudioHandler();
      event.reply("Leaving.").queue();
    }

  }
}
