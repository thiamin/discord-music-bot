package org.jordan.commands;

import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import org.jordan.Bot;
import org.jordan.BotStateManager;
import org.jordan.GuildManager;
import org.jordan.commands.InterfaceCmd;

public class ClearCmd implements InterfaceCmd {
  public BotStateManager stateManager;

  public ClearCmd(BotStateManager stateManager) {
    this.stateManager = stateManager;
  }


  @Override
  public SlashCommandData getSlashCommand() {
    return Commands.slash("clear", "clears the current track queue");
  }

  @Override
  public void trigger(SlashCommandInteractionEvent event) {
    var guildManager = stateManager.getGuildManager(event.getGuild().getIdLong());
    if (guildManager == null || guildManager.getGuildAudioHandler()
        == null) {
      event.reply("Bot not in any audio channels.").queue();
    } else {
      guildManager.getGuildAudioHandler().clearQueue();
      event.reply("Clearing queue.").queue();
    }
  }
}
