package org.jordan.commands;

import java.util.ArrayList;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.interactions.InteractionHook;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import net.dv8tion.jda.api.interactions.components.buttons.ButtonStyle;
import org.jordan.BotStateManager;
import org.jordan.GuildManager;
import org.jordan.subsonic.SubsonicApiWrapper;
import org.jordan.subsonic.SubsonicAudioItem;
import org.jordan.subsonic.SubsonicAudioTypes;
import org.jordan.subsonic.json.SearchResult;
import org.jordan.tools.SubsonicTools;
import org.jordan.ui.SelectOptionBuilder;
import org.jordan.ui.UiCallback;
import org.jordan.ui.UiListener;
import org.jordan.ui.UiOption;

public class SubsonicCmd implements InterfaceCmd {

  public final int MAX_ALBUM_COUNT = 10;
  public final int MAX_SONG_COUNT = 10;
  private final UiListener uiListener;
  private final BotStateManager stateManager;
  private final SubsonicApiWrapper subsonic;

  public SubsonicCmd(BotStateManager stateManager, UiListener uiListener,
      SubsonicApiWrapper subsonic) {
    this.stateManager = stateManager;
    this.uiListener = uiListener;
    this.subsonic = subsonic;
  }

  @Override
  public SlashCommandData getSlashCommand() {
    return Commands.slash("subsonic", "search subsonic for songs and albums")
        .addOption(OptionType.STRING, "query", "search query", true)
        .addOption(OptionType.INTEGER, "albumcount", "number of albums to show", false)
        .addOption(OptionType.INTEGER, "songcount", "number of songs to show", false);
  }

  @Override
  public void trigger(SlashCommandInteractionEvent event) {

    if (event.getOption("query") == null) {
      event.reply("command received malformed request.").queue();
    }

    GuildManager guildManager = stateManager.getGuildManager(event.getGuild().getIdLong());
    if (guildManager == null || guildManager.getGuildAudioHandler() == null) {
      event.reply("Bot not in any audio channels.").queue();
      return;
    }

    SearchResult searchResult = subsonic.search(event.getOption("query").getAsString());
    InteractionHook sentMessage = event.deferReply().complete();
    createSubsonicGui(sentMessage, searchResult);
  }

  private void createSubsonicGui(InteractionHook sentMessage, SearchResult searchResult) {
    var songs = searchResult.getSong();
    if (songs != null && songs.size() > MAX_SONG_COUNT) {
      songs = songs.subList(0, MAX_SONG_COUNT);
    }

    var albums = searchResult.getAlbum();
    if (albums != null && albums.size() > MAX_ALBUM_COUNT) {
      albums = albums.subList(0, MAX_ALBUM_COUNT);
    }

    if (songs == null && albums == null) {
      sentMessage.editOriginal("No songs or albums found").queue();
      return;
    }

    StringBuilder message = new StringBuilder();
    var options = new ArrayList<UiOption<SubsonicAudioItem>>();
    int i = 1;
    message.append("**Songs**\n");
    if (songs != null) {
      for (var song : songs) {
        message.append("**").append(i).append("**: ").append(SubsonicTools.getSongAsMessage(song))
            .append("\n");
        options.add(new UiOption<>(ButtonStyle.PRIMARY, String.valueOf(i),
            new SubsonicAudioItem(song.getId(), SubsonicAudioTypes.SONG)));
        i++;
      }
    } else {
      message.append("No songs found\n");
    }

    message.append("**Albums**\n");
    if (albums != null) {
      for (var album : albums) {
        message.append("**").append(i).append("**: ").append(SubsonicTools.getAlbumAsMessage(album))
            .append("\n");
        options.add(new UiOption<>(ButtonStyle.PRIMARY, String.valueOf(i),
            new SubsonicAudioItem(album.getId(), SubsonicAudioTypes.ALBUM)));
        i++;
      }
    } else {
      message.append("No albums found\n");
    }

    SelectOptionBuilder<SubsonicAudioItem> selectOptionBuilder = new SelectOptionBuilder<>(
        uiListener, sentMessage, new SubsonicCallback(sentMessage));
    selectOptionBuilder.addMessageText(message.toString());
    selectOptionBuilder.addUiOption(options);
    selectOptionBuilder.addUiOption(new UiOption<>(ButtonStyle.DANGER, "X", null));
    selectOptionBuilder.send();
    //sentMessage.editOriginal(message.toString()).queue();
  }

  private class SubsonicCallback implements UiCallback<SubsonicAudioItem> {

    private InteractionHook sentMessage;

    public SubsonicCallback(InteractionHook sentMessage) {
      this.sentMessage = sentMessage;
    }

    @Override
    public void onButtonClick(SubsonicAudioItem value, ButtonInteractionEvent event) {

      var guildManager = stateManager.getGuildManager(
          sentMessage.getInteraction().getGuild().getIdLong());
      if (guildManager == null) {
        return;
      }

      if (guildManager.getGuildAudioHandler() == null) {
        return;
      }

      if (value.getType() == SubsonicAudioTypes.SONG) {
        var song = subsonic.getSourceManager().loadSong(value.getId());
        guildManager.getGuildAudioHandler().addTrack(song);
        return;
      }

      if (value.getType() == SubsonicAudioTypes.ALBUM) {
        var playlist = subsonic.getSourceManager().loadAlbum(value.getId());
        guildManager.getGuildAudioHandler().addTrack(playlist);
        return;
      }

      //var trackAsString = value.getAsString();
      //System.out.println(trackAsString);
      //guildManager.getGuildAudioHandler().addTrack(trackAsString);
    }
  }
}
