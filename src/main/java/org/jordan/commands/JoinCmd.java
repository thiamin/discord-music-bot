package org.jordan.commands;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.channel.concrete.VoiceChannel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import net.dv8tion.jda.api.managers.AudioManager;
import org.jordan.BotStateManager;
import org.jordan.GuildManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class JoinCmd implements InterfaceCmd {

  private final BotStateManager stateManager;
  private final Logger log = LoggerFactory.getLogger(JoinCmd.class);

  public JoinCmd(BotStateManager stateManager) {
    this.stateManager = stateManager;
  }

  @Override
  public void trigger(SlashCommandInteractionEvent event) {
    log.info("JoinCmd Triggered");
    final Guild guild = event.getGuild();
    assert guild != null; /* guild should never be null (checked in event loop)*/
    final long guildId = guild.getIdLong();
    final Member user = event.getMember();
    VoiceChannel voiceChannel;

    event.reply("Joining channel").queue();

    if (user == null) {
      return;
    }

    voiceChannel = searchInVoiceChannels(user.getIdLong(), guild);
    AudioManager audioManager = guild.getAudioManager();

    if (audioManager.getConnectedChannel() != null) {
      event.getHook().sendMessage("Bot already running in another channel").queue();
      //return;
    } else if (voiceChannel == null) {
      event.getHook().sendMessage("User" + user.getAsMention() + " not found in any channels")
          .queue();
      return;
    }

    GuildManager guildManager = stateManager.getGuildManager(guild.getIdLong());
    if (guildManager == null) {
      guildManager = stateManager.createGuildManager(guildId, event.getMessageChannel());
    }

    guildManager.joinVoiceChannel(voiceChannel);

  }

  @Override
  public SlashCommandData getSlashCommand() {
    return Commands.slash("join", "join current audio session");
  }

  private VoiceChannel searchInVoiceChannels(long id, Guild guild) {

    if (id == 0 || guild == null) {
      return null;
    }

    final List<VoiceChannel> channels = guild.getVoiceChannels();
    for (VoiceChannel c : channels) {
      for (Member m : c.getMembers()) {
        if (m.getIdLong() == id) {
          return c;
        }
      }
    }

    return null;
  }

}
