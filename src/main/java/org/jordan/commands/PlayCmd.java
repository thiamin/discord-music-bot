package org.jordan.commands;

import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import org.jordan.BotStateManager;
import org.jordan.GuildManager;
import org.jordan.audio.GuildAudioHandler;

public class PlayCmd implements InterfaceCmd {

  private final BotStateManager stateManager;

  public PlayCmd(BotStateManager stateManager) {
    this.stateManager = stateManager;
  }

  @Override
  public SlashCommandData getSlashCommand() {
    return Commands.slash("play", "Play audio track(s)")
        .addOption(OptionType.STRING, "url", "url", true);
  }

  @Override
  public void trigger(SlashCommandInteractionEvent event) {

    if (event.getOption("url") == null) {
      event.reply("Malformed request.").queue();
      return;
    }

    GuildManager guildManager  = stateManager.getGuildManager(event.getGuild().getIdLong());
    if (guildManager == null || guildManager.getGuildAudioHandler() == null) {
      event.reply("Bot not in any audio channels.").queue();
      return;
    }

    // TODO: Use embeds to display url, title, etc, in callback.
    event.reply("Playing").queue();
    GuildAudioHandler guildAudioHandler = guildManager.getGuildAudioHandler();
    guildAudioHandler.addTrack(event.getOption("url").getAsString());
  }
}
