package org.jordan.commands;

import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;

public interface InterfaceCmd {

  SlashCommandData getSlashCommand();

  void trigger(SlashCommandInteractionEvent event);
}
