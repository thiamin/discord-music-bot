package org.jordan.commands;

import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import org.jordan.BotStateManager;
import org.jordan.GuildManager;

public class VolumeCmd implements InterfaceCmd {

  private final BotStateManager stateManager;

  public VolumeCmd(BotStateManager stateManager) {
    this.stateManager = stateManager;
  }

  @Override
  public SlashCommandData getSlashCommand() {
    return Commands.slash("vol", "set volume of player")
        .addOption(OptionType.INTEGER, "level", "new volume level", true);
  }

  @Override
  public void trigger(SlashCommandInteractionEvent event) {

    if (event.getOption("level") == null) {
      event.reply("Malformed request.").queue();
      return;
    }

    GuildManager guildManager = stateManager.getGuildManager(event.getGuild().getIdLong());
    if (guildManager == null || guildManager.getGuildAudioHandler() == null) {
      event.reply("Bot not in any audio channels.").queue();
      return;
    }

    guildManager.getGuildAudioHandler().setVolume(event.getOption("level").getAsInt());
    event.reply("Setting volume").queue();

  }
}
