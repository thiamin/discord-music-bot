package org.jordan.commands;

import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import org.jordan.BotStateManager;

public class PauseCmd implements InterfaceCmd {

  private final BotStateManager stateManager;

  public PauseCmd(BotStateManager stateManager) {
    this.stateManager = stateManager;
  }

  @Override
  public SlashCommandData getSlashCommand() {
    return Commands.slash("pause", "Toggle pause on music player");
  }

  @Override
  public void trigger(SlashCommandInteractionEvent event) {
    var guildManager = stateManager.getGuildManager(event.getGuild().getIdLong());
    if (guildManager == null || guildManager.getGuildAudioHandler() == null) {
      event.reply("Bot not in any audio channels.").queue();
    } else {
      var newPauseState = !guildManager.getGuildAudioHandler().isPaused();
      guildManager.getGuildAudioHandler().setPaused(newPauseState);
      var stateString = "paused";
      if (newPauseState) {
        stateString = "unpaused";
      }
      event.reply("Music player " + stateString).queue();
    }
  }


}
