package org.jordan.commands;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.interactions.InteractionHook;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import net.dv8tion.jda.api.interactions.components.buttons.ButtonStyle;
import org.jordan.BotStateManager;
import org.jordan.GuildManager;
import org.jordan.audio.GuildAudioHandler;
import org.jordan.tools.AudioTrackTools;
import org.jordan.ui.SelectOptionBuilder;
import org.jordan.ui.UiCallback;
import org.jordan.ui.UiListener;
import org.jordan.ui.UiOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class YouTubeCmd implements InterfaceCmd {

  private static final int MAX_YOUTUBE_SEARCH_RESULTS = 10;
  private final BotStateManager stateManager;
  private final UiListener uiListener;
  Logger log = LoggerFactory.getLogger(YouTubeCmd.class);

  public YouTubeCmd(BotStateManager stateManager, UiListener uiListener) {
    this.stateManager = stateManager;
    this.uiListener = uiListener;
  }

  @Override
  public SlashCommandData getSlashCommand() {
    return Commands.slash("ytsearch", "Search youtube for audio tracks")
        .addOption(OptionType.STRING, "query", "query", true);
  }

  @Override
  public void trigger(SlashCommandInteractionEvent event) {
    log.info("YouTubeCmd Triggered.");

    if (event.getOption("query") == null) {
      event.reply("command received malformed request.").queue();
    }

    GuildManager guildManager  = stateManager.getGuildManager(event.getGuild().getIdLong());
    if (guildManager == null || guildManager.getGuildAudioHandler() == null) {
      event.reply("Bot not in any audio channels.").queue();
      return;
    }

    GuildAudioHandler guildAudioHandler = guildManager.getGuildAudioHandler();

    InteractionHook sentMessage = event.deferReply().complete();

    String search = "ytsearch: " + event.getOption("query").getAsString();

    stateManager.getPlayerManager().loadItem(search, new AudioLoadResultHandler() {
      @Override
      public void trackLoaded(AudioTrack track) {
        sentMessage.editOriginal("Found 1 search result: " + AudioTrackTools.getAudioTrackAsMessage(track)).queue();
        guildAudioHandler.addTrack(track);
      }

      @Override
      public void playlistLoaded(AudioPlaylist playlist) {
        createYoutubeGui(sentMessage, playlist);
      }

      @Override
      public void noMatches() {
        sentMessage.editOriginal("Could not find any search results").queue();
      }

      @Override
      public void loadFailed(FriendlyException exception) {
        sentMessage.editOriginal("Something went wrong searching youtube. Try again later.").queue();
      }
    });
  }

  private void createYoutubeGui(InteractionHook sentMessage, AudioPlaylist playlist) {

    SelectOptionBuilder<AudioTrack> newMessage = new SelectOptionBuilder<AudioTrack>(uiListener,
        sentMessage, new UiCallback<AudioTrack>() {
      @Override
      public void onButtonClick(AudioTrack value, ButtonInteractionEvent event) {
        if (value == null) {
          return;
        }

        var guildManager = stateManager.getGuildManager(sentMessage.getInteraction().getGuild().getIdLong());
        if (guildManager == null) {
          log.error("Guild manager destroyed before UiCallback deregistered.");
          return;
        }

        if (guildManager.getGuildAudioHandler() == null) {
          log.error("Guild audio manager destroyed before UiCallback deregistered.");
          return;
        }

        guildManager.getGuildAudioHandler().addTrack(value);

      }
    });


    List<AudioTrack> audioTracks = playlist.getTracks();
    ArrayList<UiOption<AudioTrack>> options = new ArrayList<>();
    StringBuilder stringBuilder = new StringBuilder();
    // TODO: FIX AWFUL FOR LOOP!
    int loopEndIndex = Math.min(audioTracks.size(), MAX_YOUTUBE_SEARCH_RESULTS);
    for (int i = 0; i < loopEndIndex; i++) {
      var at = audioTracks.get(i);
      UiOption<AudioTrack> uiOption = new UiOption<>(ButtonStyle.PRIMARY,String.valueOf(i+1), at);
      options.add(uiOption);
      stringBuilder.append("**").append(i+1).append(":** ").append(AudioTrackTools.getAudioTrackAsMessage(at)).append("\n");
    }

    newMessage.addMessageText(stringBuilder.toString());
    newMessage.addUiOption(options);
    newMessage.addUiOption(new UiOption<AudioTrack>(ButtonStyle.DANGER,"X", null));
    newMessage.send();
  }
}
