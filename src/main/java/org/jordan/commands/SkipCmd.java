package org.jordan.commands;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import org.jordan.BotStateManager;
import org.jordan.GuildManager;
import org.jordan.tools.AudioTrackTools;

public class SkipCmd implements InterfaceCmd {

  private final BotStateManager stateManager;

  public SkipCmd(BotStateManager stateManager) {
    this.stateManager = stateManager;
  }

  @Override
  public SlashCommandData getSlashCommand() {
    return Commands.slash("skip", "Play next audio track").addOption(OptionType.INTEGER, "n", "number of tracks to skip", false);
  }

  @Override
  public void trigger(SlashCommandInteractionEvent event) {
    GuildManager guildManager = stateManager.getGuildManager(event.getGuild().getIdLong());

    if (guildManager == null || guildManager.getGuildAudioHandler() == null) {
      event.reply("Bot not in any audio channel").queue();
      return;
    }

    int tracksToSkip = 1;
    if (event.getOption("n") != null) {
      tracksToSkip = event.getOption("n").getAsInt();
    }

    if (tracksToSkip < 1) {
      event.reply("Cannot skip a negative number of tracks").queue();
    }

    int skipCount = guildManager.getGuildAudioHandler().skipTrack(tracksToSkip);

    if (skipCount == 0) {
      event.reply("Playlist already empty.").queue();
    } else {
      event.reply("Skipping " + skipCount + " tracks").queue();
    }
  }
}
