package org.jordan;

import java.util.Collection;
import java.util.HashMap;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jordan.commands.InterfaceCmd;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CommandListener extends ListenerAdapter {


  private final Logger log = LoggerFactory.getLogger(CommandListener.class);
  private final HashMap<String, InterfaceCmd> commandListenerHashMap = new HashMap<>();

  public void registerCommand(InterfaceCmd cmd) {
    this.commandListenerHashMap.put(cmd.getSlashCommand().getName(), cmd);
  }

  public void registerCommand(Collection<InterfaceCmd> cmd) {
    for (var c : cmd) {
      this.registerCommand(c);
    }
  }

  @Override
  public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
    String eventName = event.getName();
    log.debug("Slash command received. Name: '" + eventName + "'");

    if (event.getUser().isBot()) {
      event.reply("Bots cannot call music commands").queue();
      return;
    }

    if (event.getGuild() == null) {
      event.reply("Cannot use bot in DMs").queue();
      return;
    }

    var triggeredCommand = commandListenerHashMap.get(eventName);
    if (triggeredCommand != null) {
      triggeredCommand.trigger(event);
      return;
    }

    log.error("Slash command has no register handler");
    event.reply("Command not yet added").queue();
  }

}